# jpress社区插件_jpress论坛插件_论坛系统

#### Description
汲于jpress开发的社区论坛插件,一个插件，就是一个完整的社区解决方案，给自己的会员一个聚集的地方。免去了从头开发的极大工作量，经过大量实验和验证，稳定且轻便。包含浏览，发帖，置顶，置顶样式编辑，VIP标识等 永久免费更新,详尽设置教程,装卸方便,功能强大,灵活方便,安全可靠

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
